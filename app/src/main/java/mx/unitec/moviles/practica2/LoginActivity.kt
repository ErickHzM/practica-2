package mx.unitec.moviles.practica2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class LoginActivity : AppCompatActivity() {

    private lateinit var editTextTextEmailAddress: EditText
    private lateinit var editTextTextPassword: EditText
    private lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        editTextTextEmailAddress = findViewById(R.id.editTextTextEmailAddress)
        editTextTextPassword = findViewById(R.id.editTextTextPassword)
        button = findViewById(R.id.bottom)

        button.setOnClickListener{
            if(editTextTextEmailAddress.text.isEmpty()) {
                editTextTextEmailAddress.error = getString(R.string.error_text)
                return@setOnClickListener
            }

            if(editTextTextPassword.text.isEmpty()) {
                editTextTextPassword.error = getString(R.string.error_text)
                return@setOnClickListener
            }
            Toast.makeText(this,
                "${R.string.error_text} ${editTextTextEmailAddress.text}",
                Toast.LENGTH_SHORT).show()
        }
    }